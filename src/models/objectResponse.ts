export interface ObjectResponse {
    kind: string;
    totalItems: number;
    items: []
}