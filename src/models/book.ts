export interface Book {
    id: number;
    title: string;
    img: string;
    price: number;
    quantity?: number;
}

export const BOOKS = [
    {
        id: 1,
        title: "Harry Potter and the Philosopher's Stone",
        img: "/assets/1.png",
        price: 5
    },
    {
        id: 2,
        title: "Harry Potter and the Chamber of Secrets",
        img: "/assets/2.png",
        price: 5
    },
    {
        id: 3,
        title: "Harry Potter and the Prisoner of Azkaban",
        img: "/assets/3.png",
        price: 5
    },
    {
        id: 4,
        title: "Harry Potter and the Goblet of Fire",
        img: "/assets/4.png",
        price: 5
    },
    {
        id: 5,
        title: "Harry Potter and the Order of the Phoenix",
        img: "/assets/5.png",
        price: 5
    },
    {
        id: 6,
        title: "Harry Potter and the Half-Blood Prince",
        img: "/assets/6.png",
        price: 5
    },
    {
        id: 7,
        title: "Harry Potter and the Deathly Hallows",
        img: "/assets/7.png",
        price: 5
    }
]

// export var basket = {booklist: [    {
//     id: 6,
//     title: "Tome 6",
//     img: "/assets/6.png",
//     price: 5
// }], total: 1}