import { Book } from './book';

export interface Basket {
    booklist: Book[];
}