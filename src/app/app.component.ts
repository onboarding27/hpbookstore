import { Component } from '@angular/core';
import { Book } from '../models/book';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private toastr: ToastrService) { }
  title = 'my-app';
  books: Array<Book> = [];
  basket = { booklist: this.books }
  addBook(newBook: Book) {
    const book = this.basket.booklist.find(obj => obj.id == newBook.id)
    if (book) {
      book.quantity = (book.quantity||0)+1
    } else {
      this.basket.booklist.push({...newBook, quantity: 1})
    }
    this.toastr.success(newBook.title + " was added to the basket")
  }
  removeBook(book: Book) {
    const bookIndex = this.basket.booklist.findIndex(obj => obj.id == book.id)
    if (bookIndex > -1) {
      const bookInBasket = this.basket.booklist[bookIndex]
      if (typeof bookInBasket.quantity !== "undefined") {
        bookInBasket.quantity--
        if (bookInBasket.quantity == 0) {
          this.basket.booklist.splice(bookIndex, 1)
        }
      }
      this.toastr.error(book.title + " was removed from the basket")

    }
  }
}
