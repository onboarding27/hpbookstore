import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Book } from '../../models/book';
import { Basket } from '../../models/basket';

@Component({
	selector: 'app-basket',
	templateUrl: './basket.component.html',
	styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

	@Input() basket: Basket = { booklist: [] };

	constructor() { }

	ngOnInit(): void {
	}
	ngOnChanges(): void { }
	@Output() newBookRemoved = new EventEmitter<Book>();
	removeBook(book: Book) {
		this.newBookRemoved.emit(book)
	}

}


