import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Book } from 'src/models/book';
import { BookCardComponent } from './book-card.component';

describe('BookCardComponent', () => {

  let component: BookCardComponent;
  let fixture: ComponentFixture<BookCardComponent>;
  let divCardQuantity = HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCardComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(BookCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('quantity should appear if != 0', () => {
    component.book = {
      id: 1,
      title: "Testing Book",
      img: "/assets/placeholder.jpg",
      price: 5,
      quantity: 12,
    }
    fixture.detectChanges();
    // divCardQuantity = fixture.debugElement.query(By.css(".card-quantity-bubble")).nativeElement
    let divCardQuantity = document.getElementsByClassName("card-quantity-bubble")
    expect(divCardQuantity[0].textContent).toContain(component.book.quantity)

  })

  // it('quantity should appear if != 0'), () => {
  //   const fixture = TestBed.createComponent(BookCardComponent);
  //   const component = fixture.componentInstance;
  //   component.book = {
  //     id: 1,
  //     title: "Testing Book",
  //     img: "/assets/placeholder.jpg",
  //     price: 5,
  //     quantity: 12,
  //   }
  //   expect(fixture.debugElement.query(By.css('.card-quantity'))).toBeDefined()
  // }
});
