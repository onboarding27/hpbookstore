import { Injectable } from '@angular/core';
import { Book } from 'src/models/book';
import { BOOKS } from 'src/models/book';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ObjectResponse } from 'src/models/objectResponse';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }



  getBooks(): Observable<Book[]> {
    const books = of(BOOKS)
    return books
  }
  getBooksFromApi(): Observable<Book[]> {
    // const url = "https://www.googleapis.com/books/v1/volumes?q=nauthor:Jason%20Pargin"
    const url = "https://www.googleapis.com/books/v1/volumes?q=harry+potter+inauthor:J.K.%20Rwoling+inpublisher:Gallimard-Jeunesse"
    let booksApi: Array<Book> = [];
    let newBook: Book
    this.http.get<ObjectResponse>(url).subscribe((Response) => {
      const response: ObjectResponse = Response
      response.items.forEach((item: any) => {
        let img
        if (item.volumeInfo.imageLinks) {
          img = item.volumeInfo.imageLinks.thumbnail
        } else {
          img = "/assets/placeholder.jpg"
        }
        newBook = {
          id: item.id,
          title: item.volumeInfo.title,
          img: img,
          price: 5
        }
        booksApi.push(newBook)
      });
    })
    return of(booksApi)
  }
}

