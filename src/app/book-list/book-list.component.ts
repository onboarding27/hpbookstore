import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { __values } from 'tslib';
import { Book, BOOKS } from '../../models/book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  constructor(private bookService: BookService) {}

  ngOnInit(): void {
    this.getBooks()
  }
  books : Book[] = [];
  @Output() newBookAdded = new EventEmitter<Book>();
  addBook(book: Book) {
    this.newBookAdded.emit(book)
  }
  getBooks(): void {
   this.bookService.getBooksFromApi()
   .subscribe(books => this.books = books)
  }

}
